package org.hitesh.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hitesh.domain.PostalCode;
import org.hitesh.service.PostalDataService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
@RequestMapping("/main")
public class MainController {

	protected static Logger logger = Logger.getLogger("controller");
	
	@Resource(name="postalDataService")
	private PostalDataService postalDataService;
 
    /**
     * Selects between city name and postal code
     * 
     * @return  the name of the JSP page
     */
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String getPostalCode(Model model) {
    	
    	logger.debug("Welcome Page");

    	return "postalpage";
    	}
	
    @RequestMapping(value = "/searchby", method = RequestMethod.GET)
    public String searchingBy(@RequestParam(value="search", required=true) String var,Model model) {
    	logger.debug("Received request to show for postal code search query");
    
    	PostalCode temp = new PostalCode();
    	model.addAttribute("Value",temp);
    	if(var.equals("Code"))
    		return "codepage";
    	else if(var.equals("City"))
    		return "citypage";
    	return "statepage";
	}
    
    @RequestMapping(value = "/searchByCode", method = RequestMethod.GET)
    public String giveCityName(@RequestParam(value="code", required=true) String code, 
    										Model model) {
   
		logger.debug("Received request to show city for given code");
		
		List<PostalCode> pc = postalDataService.getCity(code);
    	
		model.addAttribute("cityName", pc);
    	
    	// This will resolve to /WEB-INF/jsp/deletedpage.jsp
		return "cityNamePage";
	}
    @RequestMapping(value = "/searchByCity", method = RequestMethod.GET)
    public String giveZipCode(@RequestParam(value="place", required=true) String var, 
    										Model model) {
   
		logger.debug("Received request to show Zip Code for given city name");
		
		
		List<PostalCode> pc = postalDataService.getCode(var);
    	
		model.addAttribute("post", pc);
    	
		return "showcodepage";
	}
    @RequestMapping(value = "/searchByState", method = RequestMethod.GET)
    public String giveCities(@RequestParam(value="admin1name", required=true) String var, 
    										Model model) {
   
		logger.debug("Received request to show Zip Code for given city name");
		
		List<PostalCode> pc = postalDataService.getCities(var);
    	
		model.addAttribute("cities", pc);
    	
		return "stateResultPage";
	}
}
