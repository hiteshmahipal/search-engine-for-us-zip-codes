package org.hitesh.domain;

public class PostalCode{

	private String pid;
	private String country;
	private String code;
	private String place;
	private String admin1name;
	private String admin1code;
	private String admin2name;
	private String admin2code;
	private String latitude;
	private String longitude;
	private String accuracy;
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getAdmin1name() {
		return admin1name;
	}
	public void setAdmin1name(String admin1name) {
		this.admin1name = admin1name;
	}
	public String getAdmin1code() {
		return admin1code;
	}
	public void setAdmin1code(String admin1code) {
		this.admin1code = admin1code;
	}
	public String getAdmin2name() {
		return admin2name;
	}
	public void setAdmin2name(String admin2name) {
		this.admin2name = admin2name;
	}
	public String getAdmin2code() {
		return admin2code;
	}
	public void setAdmin2code(String admin2code) {
		this.admin2code = admin2code;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

	}
