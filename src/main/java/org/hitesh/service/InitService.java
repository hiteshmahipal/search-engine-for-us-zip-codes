package org.hitesh.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.UUID;

import javax.annotation.Resource;



//import org.apache.catalina.core.ApplicationContext;
import org.apache.log4j.Logger;
import org.hitesh.domain.PostalCode;
//import org.springframework.data.document.mongodb.MongoTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.mongodb.core.MongoTemplate;

@Transactional
public class InitService {

	protected static Logger logger = Logger.getLogger("service");
	
	@Resource(name="mongoTemplate")
	private MongoTemplate mongoTemplate;

	private void init() {
		// Populate our MongoDB database
		logger.debug("Init MongoDB users");
	
		// Drop existing collection
		mongoTemplate.dropCollection("Codes");
		mongoTemplate.createCollection("Codes");
		
	    
	    String file_name = "C:/Users/Hitesh/workspace/US-Postal-Database/USedited.txt";
	    String[] str = null;
		try{
			ReadFile tempo = new ReadFile(file_name);
			
			BufferedReader br = tempo.getBufferedReader();
			int n = 0;
			while (br.readLine() != null) n++;
			br.close();
			
			n=n/500;
			n++;
			
			ReadFile file = new ReadFile(file_name);
			for(int j=0; j<n; j++)
			{
				str = file.OpenFile();
				for(int i=0; i<str.length; i++)
				{
					PostalCode temp = new PostalCode();
					String[] st = str[i].split("\t", 10);
					temp.setPid(UUID.randomUUID().toString());
					temp.setCountry(st[0]);
					temp.setCode(st[1]);
					temp.setPlace(st[2]);
					temp.setAdmin1name(st[3]);
					temp.setAdmin1code(st[4]);
					temp.setAdmin2name(st[5]);
					temp.setAdmin2code(st[6]);
					temp.setLatitude(st[7]);
					temp.setLongitude(st[8]);
					temp.setAccuracy(st[9]);
					mongoTemplate.insert(temp,"Codes");
				}
				str=null;
			}
		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}

	}
}
