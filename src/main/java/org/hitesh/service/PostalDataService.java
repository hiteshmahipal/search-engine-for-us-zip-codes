package org.hitesh.service;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hitesh.domain.PostalCode;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;




@Service("postalDataService")
@Transactional
public class PostalDataService {

	protected static Logger logger = Logger.getLogger("service");
	
	@Resource(name="mongoTemplate")
	private MongoTemplate mongoTemplate;
	
	
	public List<PostalCode> getCity(String code) {
		logger.debug("Retrieving city name for postal code");
 
		Query query = new Query();
        query.addCriteria(Criteria.where("code").is(code));
        List<PostalCode> pc = mongoTemplate.find(query, PostalCode.class,"Codes");
        
		return pc;
	}
	public List<PostalCode> getCode(String cityname) {
		logger.debug("Retrieving city name for postal code");
 
		Query query = new Query();
        query.addCriteria(Criteria.where("place").regex(cityname,"i"));
        query.with(new Sort(Sort.Direction.ASC,"place"));
        List<PostalCode> pc = mongoTemplate.find(query, PostalCode.class,"Codes");
		return pc;
	}
	public List<PostalCode> getCities(String statename) {
		logger.debug("Retrieving city name for postal code");
 
		Query query = new Query();
        query.addCriteria(Criteria.where("admin1name").regex(statename,"i"));
        query.with(new Sort(Sort.Direction.ASC,"place"));
        List<PostalCode> pc = mongoTemplate.find(query, PostalCode.class,"Codes");
		return pc;
	}
}
