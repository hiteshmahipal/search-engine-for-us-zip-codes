package org.hitesh.service;

import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.Vector;

public class ReadFile {
	private static final String NULL = null;
	private static String path;
	private static BufferedReader textReader;
	
	public BufferedReader getBufferedReader()
	{
		return textReader;
	}
	
	public ReadFile(String file_path) throws IOException
	{
		path = file_path;
		FileReader fr = new FileReader(path);
		textReader =  new BufferedReader(fr);
	}
	
	public static String[] OpenFile() throws IOException{
		
		int numberOfLines = 500;
		String[] textData=null;
		String temp;
		Vector<String> v = new Vector<String>();
		int i=0;
		while(i < numberOfLines && (temp = textReader.readLine())!=null) {
			i++;
			v.add(temp);
		}
		v.trimToSize();
		textData = new String[v.size()];
		v.toArray(textData);
		return textData;
	}
}
