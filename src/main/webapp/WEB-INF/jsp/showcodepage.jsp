<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search Engine</title>
</head>
<body>


 <table style="border: 1px solid; width: 500px; text-align:center">
	<thead style="background:#fcf">
		<tr>
			<th>Country</th>
			<th>Code</th>
			<th>Place</th>
			<th>admin1</th>
			<th>admin1Code</th>
			<th>admin2</th>
			<th>admin2Code</th>
			<th>latitude</th>
			<th>longitude</th>
			<th>Accuracy</th>
			<th colspan="3"></th>
		</tr>
	</thead>
	<tbody>
	<c:forEach items="${post}" var="p">
		<tr>
			<td><c:out value="${p.country}" /></td>
			<td><c:out value="${p.code}" /></td>
			<td><c:out value="${p.place}" /></td>
			<td><c:out value="${p.admin1name}" /></td>
			<td><c:out value="${p.admin1code}" /></td>
			<td><c:out value="${p.admin2name}" /></td>
			<td><c:out value="${p.admin2code}" /></td>
			<td><c:out value="${p.latitude}" /></td>
			<td><c:out value="${p.longitude}" /></td>
			<td><c:out value="${p.accuracy}" /></td>
		</tr>
	</c:forEach>
	</tbody>
</table>

</body>
</html>