<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search Engine</title>
</head>
<body>


<c:url var="saveUrl" value="/hitesh/main/searchByCode?code=${Value.code}$" />
<form:form modelAttribute="Value" method="GET" action="${saveUrl}">
	<table>
		<tr>
			<td><form:label path="code">Code</form:label></td>
			<td><form:input path="code"/></td>
		</tr>
	</table>
	
	<input type="submit" value="Submit" />
</form:form>

</body>
</html>