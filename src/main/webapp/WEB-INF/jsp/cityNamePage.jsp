<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search Engine</title>
</head>
<body>


 <table style="border: 1px solid; width: 500px; text-align:center">
	<thead style="background:#fcf">
		<tr>
			<th>Country</th>
			<th>Code</th>
			<th>Place</th>
			<th>admin1</th>
			<th>admin1Code</th>
			<th>admin2</th>
			<th>admin2Code</th>
			<th>latitude</th>
			<th>longitude</th>
			<th>Accuracy</th>
			<th colspan="3"></th>
		</tr>
	</thead>
	<tbody>
	<c:forEach items="${cityName}" var="post">
		<tr>
			<td><c:out value="${post.country}" /></td>
			<td><c:out value="${post.code}" /></td>
			<td><c:out value="${post.place}" /></td>
			<td><c:out value="${post.admin1name}" /></td>
			<td><c:out value="${post.admin1code}" /></td>
			<td><c:out value="${post.admin2name}" /></td>
			<td><c:out value="${post.admin2code}" /></td>
			<td><c:out value="${post.latitude}" /></td>
			<td><c:out value="${post.longitude}" /></td>
			<td><c:out value="${post.accuracy}" /></td>
		</tr>
	</c:forEach>
	</tbody>
</table>

</body>
</html>